# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from . import common as gvc
from . import uml


ns_comp = dict(shape='component', style='filled', fillcolor=uml.yellow, color=uml.red)

def prov_intf(node, name):
    intf = gvc.add_node(
        '',
        xlabel=name,
        shape='circle', width=0.15,
        style='filled', fillcolor=uml.yellow)
    gvc.add_edge(intf, node,
                 #label=name,
                 arrowhead='none',
                 penwidth=3,
                 minlen=0
    )
    return intf

def req_intf(node, name):
    intf = gvc.add_node('', shape='none', width=0.15, style='invis', fillcolor=uml.yellow)
    gvc.add_edge(node, intf, headlabel=name, arrowhead='tee')
    return intf

def use_intf(node, intf):
    gvc.add_edge(node, intf, style='dashed') #, arrowhead='tee')


"""
import mygv
import mygv.uml_comp as comp
import mygv.uml as uml
a=gvc.add_node('A', **comp.ns_comp)
b=gvc.add_node('B', **comp.ns_comp)
ia=comp.prov_intf(a,'iA')
comp.use_intf(b, ia)
gvc.draw('test_comp.pdf', prog='dot')
"""
