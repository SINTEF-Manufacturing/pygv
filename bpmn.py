# coding=utf-8

"""Some styles and symbols from https://www.omg.org/spec/BPMN/2.0.2/
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2023-2024"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


actor_node_style = dict(xlabel='', shape='none', image='actor.png',
                        peripheries=0)

activity_node_style = dict(shape="box", width=1.25, height=0.75,
                            style='rounded,filled')
operation_node_style = activity_node_style

data_object_style = dict(shape="note", style="filled",
                       fillcolor="#f0f0f0")
data_node_style = data_object_style
data_store_style = dict(shape="cylinder", style="filled",
                       fillcolor="#f0f0f0")
gateway_node_style = dict(label='X', shape='diamond', fillcolor='#ffcc77',
                           style='filled', regular=True)
exclusive_node_style = gateway_node_style | dict(label='X')
decision_node_style =exclusive_node_style
inclusive_node_style = gateway_node_style | dict(label='O')
parallell_node_style = gateway_node_style | dict(label='+')

area_fill_color = '#f7f7f7'
lane_style = dict(cluster=True, style='filled',
                  fillcolor=area_fill_color,
                  label='', penwidth=2)
pool_style = dict(cluster=True, style='filled',
                  fillcolor=area_fill_color,
                  label='', penwidth=2)
group_style = dict(cluster=True, style='dashed,filled',
                   fillcolor=area_fill_color,
                   label='', penwidth=2)


normal_flow_style = dict(penwidth=1.5, minlen=1, weight=10,
                         dir='both', arrowtail='none')
control_edge_style = normal_flow_style
conditional_flow_style = normal_flow_style | dict(arrowtail='odiamond')
default_flow_style = normal_flow_style | dict(arrowtail='rcurve')
message_flow_style = normal_flow_style | dict(arrowhead='onormal', arrowtail='odot',
                                              style='dashed')

association_style = dict(arrowhead='none', weight=1, minlen=0, len=0,
                         style='dotted', penwidth=1.5, fontsize=10)
association_edge_style = association_style

directed_association_style = association_style | dict(arrowhead='vee')
data_edge_style = directed_association_style
