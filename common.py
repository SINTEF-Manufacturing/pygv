# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import warnings

import pygraphviz as gv


g = gv.AGraph(directed=True,
              strict=False,
              #, rankdir='TB'
              )
#g.graph_attr['rankdir'] ='TB'
g.graph_attr['compound'] = True
# g.graph_attr['ranksep'] = 1
# g.graph_attr['concentrate'] = True
g.graph_attr['labeljust'] = 'r'
g.graph_attr['labelloc'] = 'b'
#g.graph_attr['clusterrank'] = 'local'
g.graph_attr['splines'] = True
g.graph_attr['fontname'] = 'FreeSans'
g.node_attr['fontname'] = 'FreeSans'
g.edge_attr['fontname'] = 'FreeSans'
#g.edge_attr['weight'] = 1

_nid = -1

node_attr = {}
edge_attr = {}


def add_node(label, g=g, **style):
    """Add and return a newly created node."""
    global _nid
    if type(label) in (list, tuple):
        return [add_node(l, **style) for l in label]
    _nid += 1
    g.add_node(_nid, label=label)
    n = g.get_node(_nid)
    n.attr.update(node_attr)
    n.attr.update(style)
    # Keep data for post mortem
    n.label = n.attr['label']
    return n


def del_node(n, g=g, **style):
    """Add and return a newly created node."""
    if type(n) in (list, tuple):
        for ni in n:
            del_node(ni, g=g, **style)
    # Delete all incoming edges
    for e in g.edges():
        if n in e:
            g.delete_edge(*e)
    g.delete_node(n)


def add_edge(n1, n2, **style):
    """Add and return a newly created edge."""
    edges = []
    if type(n2) in (list, tuple):
        for n2i in n2:
            edges.append(add_edge(n1, n2i, **style))
    elif type(n1) in (list, tuple):
        for n1i in n1:
            edges.append(add_edge(n1i, n2, **style))
    else:
        if n1 not in g.nodes() or n2 not in g.nodes():
            # raise Exception
            warnings.warn(f'add_edge: Nodes existence: '
                          f'\n\t{repr(n1.label)}({repr(n1)}): {n1 in g.nodes()}'
                          f'\n\t{repr(n2.label)}({repr(n2)}): {n2 in g.nodes()}',
                          UserWarning,
                          stacklevel=2)
            return None
        else:
            g.add_edge(n1, n2)
            e = g.get_edge(n1, n2)
            e.attr.update(edge_attr)
            e.attr.update(style)
            edges.append(e)
            return edges


def del_edge(n1, n2):
    """Delete edge between nodes."""
    if type(n2) in (list, tuple):
        for n2i in n2:
            del_edge(n1, n2i)
    elif type(n1) in (list, tuple):
        for n1i in n1:
            del_edge(n1i, n2)
    else:
        if g.has_edge(n1, n2):
            g.delete_edge(n1, n2)


def add_subg(name, **kwargs):
    """Add and return a newly created sub graph. If 'cluster' is a prefix
    in 'name', the subgraph will be a cluster.

    """
    return g.add_subgraph(name=name, **kwargs)


def draw(file_name, prog='dot', **kwargs):
    g.layout(prog)
    g.draw(file_name, **kwargs)
